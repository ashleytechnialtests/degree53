﻿using CheckoutClientCore.Presenter;
using ShoppingService;
using System;
using System.ServiceModel;

namespace CheckoutClientConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            BasicHttpBinding myBinding = new BasicHttpBinding();

            EndpointAddress myEndpoint = new EndpointAddress("http://localhost:54782/ShoppingService.svc");

            ChannelFactory<IShoppingService> myChannelFactory = new ChannelFactory<IShoppingService>(myBinding, myEndpoint);

            IShoppingService wcfClient = myChannelFactory.CreateChannel();

            Application consoleApplication = new Application();

            ShoppingPresenter shoppingPresenter = new ShoppingPresenter(consoleApplication, wcfClient, args);

            DebugCheckoutBasket(consoleApplication, args);

            Console.Read();
        }

        static void DebugCheckoutBasket(Application app, string[] skus)
        {
            for (int i = 0; i < skus.Length; i++)
            {
                string sku = skus[i].ToUpper();
                if (app.DisplayProducts.Exists(x => x.Sku == sku))
                {
                    app.DisplayProductsIndex = app.DisplayProducts.FindIndex(x => x.Sku == sku);
                }
            }
        }
    }
}
