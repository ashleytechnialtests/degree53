﻿using CheckoutClientCore.Model;
using CheckoutClientCore.Presenter;
using CheckoutClientCore.View;
using System;
using System.Collections.Generic;
using System.Text;

namespace CheckoutClientConsole
{
    public class Application : IShoppingView
    {
        public List<DisplayProduct> DisplayProducts { get; set; }
        public List<Product> Checkout
        {
            get
            {
                return checkout;
            }
            set
            {
                if (value.Count != 0)
                {
                    Presenter.UpdateCheckoutPricing();
                }
                checkout = value;
            }
        }
        public ShoppingPresenter Presenter { private get; set; }
        public float MultiBuySavings
        {
            get => multiBuySavings;
            set
            {
                Console.WriteLine($"savings: {value}");
                multiBuySavings = value;
            }
        }
        public float AmoundOwed
        {
            get => amoundOwed;
            set
            {
                Console.Clear();
                Console.WriteLine($"amount owed: {value}");
                amoundOwed = value;
            }
        }
        public int DisplayProductsIndex
        {
            get
            {
                return displayProductsIndex;
            }
            set
            {
                displayProductsIndex = value;
                Presenter.AddToCheckoutBasket();
            }
        }
        public int CheckoutIndex { get; set; }

        private float multiBuySavings { get; set; }
        public float amoundOwed { get; set; }

        private List<Product> checkout { get; set; }
        private int displayProductsIndex { get; set; }
    }
}
