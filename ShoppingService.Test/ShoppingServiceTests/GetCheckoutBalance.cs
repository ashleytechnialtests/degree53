﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using ShoppingService.DataModel;
using ShoppingService.DataModel.Responce;
using ShoppingService.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingService.Test.ShoppingServiceTests
{
    public class GetCheckoutBalance
    {
        private ShoppingService _shoppingService;
        private Mock<IShoppingRepository> _shoppingRepository;

        [SetUp]
        public void SetUp()
        {
            _shoppingRepository = new Mock<IShoppingRepository>();
            _shoppingService = new ShoppingService(_shoppingRepository.Object);
        }

        [Test]
        public void given_product_and_product_offer_should_return_correct_display_product_responce()
        {
            GivenProductOffers();
            GivenProducts();

            var displayProducts = _shoppingService.GetDisplayProducts();

            displayProducts[0].Should().BeEquivalentTo(new DisplayProductResponce
            {
                Sku = "A99",
                SpecialOffer = "3 for 1.3",
                Description = "Apple",
                Price = 0.5f
            });
        }

        private void GivenProductOffers()
        {
            _shoppingRepository.Setup(x => x.GetProductOffers())
                    .Returns(new List<ProductOffer>
                    {
                        new ProductOffer
                    {
                        Sku = "A99",
                        AmountOfUnits = 3,
                        Price = 1.30f
                    },
                    new ProductOffer
                    {
                        Sku = "B15",
                        AmountOfUnits = 2,
                        Price = 0.45f
                    }
                });
        }

        private void GivenProducts()
        {
            _shoppingRepository.Setup(x => x.GetProducts())
                .Returns(new List<Product>
                {
                    new Product
                {
                    Sku = "A99",
                    Description = "Apple",
                    Price = 0.50f,
                },
                new Product
                {
                    Sku = "B15",
                    Description = "Biscuits",
                    Price = 0.30f,
                },
                new Product
                {
                    Sku = "C40",
                    Description = "Coffee",
                    Price = 1.80f,
                },
                new Product
                {
                    Sku = "T23",
                    Description = "Tissues",
                    Price = 0.99f,
                }
                });
        }
    }
}
