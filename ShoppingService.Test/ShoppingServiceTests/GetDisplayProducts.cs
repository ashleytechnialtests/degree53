﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using ShoppingService.DataModel;
using ShoppingService.DataModel.Responce;
using ShoppingService.DomainModel.Requests;
using ShoppingService.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingService.Test.ShoppingServiceTests
{
    public class GetDisplayProducts
    {
        private ShoppingService _shoppingService;
        private Mock<IShoppingRepository> _shoppingRepository;

        [SetUp]
        public void SetUp()
        {
            _shoppingRepository = new Mock<IShoppingRepository>();
            _shoppingService = new ShoppingService(_shoppingRepository.Object);
        }

        [Test]
        public void given_invalid_product_should_throw_exception_with_message()
        {
            GivenProductOffers();
            GivenProducts();

            List<CheckoutBalanceRequest> checkoutBalanceRequests = new List<CheckoutBalanceRequest>
            {
                new CheckoutBalanceRequest
                {
                    Sku = "9",
                    AmountOfUnits = 2
                }
            };

            _shoppingService.Invoking(x => x.GetCheckoutBalance(checkoutBalanceRequests))
                .Should().Throw<Exception>()
                .WithMessage("Invalid product 9");
        }

        [Test]
        public void given_null_checkout_products_should_throw_exception_with_message()
        {
            GivenProductOffers();
            GivenProducts();

            _shoppingService.Invoking(x => x.GetCheckoutBalance(null))
                .Should().Throw<ArgumentException>()
                .WithMessage("Checkout Balance can't be null");
        }

        [Test]
        public void given_checkout_products_should_return_correct_amount_and_savings()
        {
            GivenProductOffers();
            GivenProducts();

            List<CheckoutBalanceRequest> checkoutBalanceRequests = new List<CheckoutBalanceRequest>
            {
                new CheckoutBalanceRequest
                {
                    Sku = "A99",
                    AmountOfUnits = 2
                }
            };

            var responce = _shoppingService.GetCheckoutBalance(checkoutBalanceRequests);

            responce.Amount.Should().Be(1);
            responce.MultiBuySavings.Should().Be(0);
        }

        [Test]
        public void given_multi_of_same_product_should_return_correct_multi_buy_savings()
        {
            GivenProductOffers();
            GivenProducts();

            List<CheckoutBalanceRequest> checkoutBalanceRequests = new List<CheckoutBalanceRequest>
            {
                new CheckoutBalanceRequest
                {
                    Sku = "A99",
                    AmountOfUnits = 4
                }
            };

            var responce = _shoppingService.GetCheckoutBalance(checkoutBalanceRequests);

            responce.MultiBuySavings.Should().Be(1.3f);
        }

        private void GivenProductOffers()
        {
            _shoppingRepository.Setup(x => x.GetProductOffers())
                    .Returns(new List<ProductOffer>
                    {
                        new ProductOffer
                    {
                        Sku = "A99",
                        AmountOfUnits = 3,
                        Price = 1.30f
                    },
                    new ProductOffer
                    {
                        Sku = "B15",
                        AmountOfUnits = 2,
                        Price = 0.45f
                    }
                });
        }

        private void GivenProducts()
        {
            _shoppingRepository.Setup(x => x.GetProducts())
                .Returns(new List<Product>
                {
                    new Product
                {
                    Sku = "A99",
                    Description = "Apple",
                    Price = 0.50f,
                },
                new Product
                {
                    Sku = "B15",
                    Description = "Biscuits",
                    Price = 0.30f,
                },
                new Product
                {
                    Sku = "C40",
                    Description = "Coffee",
                    Price = 1.80f,
                },
                new Product
                {
                    Sku = "T23",
                    Description = "Tissues",
                    Price = 0.99f,
                }
            });
        }
    }
}
