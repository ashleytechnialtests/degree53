﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ShoppingService.DataModel.Responce
{
    [DataContract]
    public class DisplayProductResponce
    {
        [DataMember]
        public string Sku { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public float Price { get; set; }
        [DataMember]
        public string SpecialOffer { get; set; }
    }
}