﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ShoppingService.DataModel.Responce
{
    [DataContract]
    public class CheckoutBalanceResponce
    {
        [DataMember]
        public float Amount { get; set; }

        [DataMember]
        public float MultiBuySavings { get; set; }
    }
}