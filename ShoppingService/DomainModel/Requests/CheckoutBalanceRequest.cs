﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ShoppingService.DomainModel.Requests
{
    [DataContract]
    public class CheckoutBalanceRequest
    {
        [DataMember]
        public string Sku { get; set; }
        [DataMember]
        public int AmountOfUnits { get; set; }
    }
}