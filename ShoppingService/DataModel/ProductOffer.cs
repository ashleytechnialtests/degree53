﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoppingService.DataModel
{
    public class ProductOffer
    {
        public string Sku { get; set; }
        public int AmountOfUnits { get; set; }
        public float Price { get; set; }
    }
}