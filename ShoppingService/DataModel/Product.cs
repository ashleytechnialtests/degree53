﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoppingService.DataModel
{
    public class Product
    {
        public string Sku { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
    }
}