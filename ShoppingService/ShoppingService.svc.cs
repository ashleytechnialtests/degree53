﻿using ShoppingService.DataModel;
using ShoppingService.DataModel.Responce;
using ShoppingService.DomainModel.Requests;
using ShoppingService.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ShoppingService
{
    public class ShoppingService : IShoppingService
    {
        private IShoppingRepository _shoppingRepository;
        
        public ShoppingService(IShoppingRepository shoppingRepository)
        {
            _shoppingRepository = shoppingRepository;
        }

        /*
         * This is more of a safety. You can't really trust the price from the client so best is to compare against databse
         */
        public CheckoutBalanceResponce GetCheckoutBalance(List<CheckoutBalanceRequest> checkoutBalanceRequest)
        {
            if (checkoutBalanceRequest == null) throw new ArgumentException("Checkout Balance can't be null");

            List<Product> products = _shoppingRepository.GetProducts();
            List<ProductOffer> productOffers = _shoppingRepository.GetProductOffers();

            float multiBuySavings = 0;
            float amountOwed = 0;

            foreach (var request in checkoutBalanceRequest)
            {
                if (products.Exists(x => x.Sku == request.Sku))
                {
                    Product product = products.Single(x => x.Sku == request.Sku);
                    
                    if (productOffers.Exists(x => x.Sku == request.Sku))
                    {
                        ProductOffer productOffer = productOffers.Single(x => x.Sku == request.Sku);

                        if (request.AmountOfUnits < productOffer.AmountOfUnits)
                        {
                            amountOwed += product.Price * request.AmountOfUnits;
                        }
                        else
                        {
                            float savingAmount = (float)Math.Floor((double)(request.AmountOfUnits / productOffer.AmountOfUnits)) * productOffer.Price;
                            amountOwed += savingAmount + ((request.AmountOfUnits % productOffer.AmountOfUnits) * product.Price);
                            multiBuySavings += savingAmount;
                        }
                    }
                    else
                    {
                        amountOwed += product.Price * request.AmountOfUnits;
                    }
                }
                else
                {
                    throw new Exception($"Invalid product {request.Sku}");
                }
            }

            return new CheckoutBalanceResponce
            {
                Amount = amountOwed,
                MultiBuySavings = multiBuySavings
            };
        }

        public List<DisplayProductResponce> GetDisplayProducts()
        {
            List<Product> products = _shoppingRepository.GetProducts();
            List<ProductOffer> productOffers = _shoppingRepository.GetProductOffers();

            return products.Select(x =>
            {
                var responce = new DisplayProductResponce
                {
                    Sku = x.Sku,
                    Description = x.Description,
                    Price = x.Price
                };

                if (productOffers.Exists(of => of.Sku == responce.Sku))
                {
                    var offer = productOffers.Single(of => of.Sku == x.Sku);
                    responce.SpecialOffer = $"{offer.AmountOfUnits} for {offer.Price}";
                }

                return responce;
            }).ToList();
        }
    }
}
