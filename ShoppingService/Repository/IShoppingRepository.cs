﻿using ShoppingService.DataModel;
using ShoppingService.DataModel.Responce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingService.Repository
{
    public interface IShoppingRepository
    {
        List<Product> GetProducts();
        List<ProductOffer> GetProductOffers();
    }
}
