﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ShoppingService.DataModel;
using ShoppingService.DataModel.Responce;

namespace ShoppingService.Repository.Concrete
{
    public class ShoppingRepository : IShoppingRepository
    {
        public List<ProductOffer> GetProductOffers()
        {
            return new List<ProductOffer>
            {
                new ProductOffer
                {
                    Sku = "A99",
                    AmountOfUnits = 3,
                    Price = 1.30f
                },
                new ProductOffer
                {
                    Sku = "B15",
                    AmountOfUnits = 2,
                    Price = 0.45f
                }
            };
        }

        public List<Product> GetProducts()
        {
            return new List<Product>
            {
                new Product
                {
                    Sku = "A99",
                    Description = "Apple",
                    Price = 0.50f,
                },
                new Product
                {
                    Sku = "B15",
                    Description = "Biscuits",
                    Price = 0.30f,
                },
                new Product
                {
                    Sku = "C40",
                    Description = "Coffee",
                    Price = 1.80f,
                },
                new Product
                {
                    Sku = "T23",
                    Description = "Tissues",
                    Price = 0.99f,
                }
            };
        }
    }
}