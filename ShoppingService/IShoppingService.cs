﻿using ShoppingService.DataModel.Responce;
using ShoppingService.DomainModel.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingService
{
    [ServiceContract]
    public interface IShoppingService
    {
        [OperationContract]
        List<DisplayProductResponce> GetDisplayProducts();

        [OperationContract]
        CheckoutBalanceResponce GetCheckoutBalance(List<CheckoutBalanceRequest> checkoutBalanceRequest);
    }
}
