# Welcome
Welcome to my technical test. Their are few projects in their because of the way code is structured. 

## Projects
ShoppingService - Is a wcf service which the checkout client will connect to do shopping.
CheckoutClientCore - containts the main logic for the client side.
CheckoutClientConsole - is the debug version where you will have to add it through command line and once application runs will display result in console.
CheckoutClientGui - winform application containing listviews. Left one will contain products while right one is checkout. To buy a item double click on it.

## How to
Shopping service will be needed to start first as the rest of project connect to it. Once it started, start either gui or console (you will need to add command line arguments though).

## To do

- Still needs more error checking. As it never checks the wcf service
- Only Service part has basic unit tests. So more unit tests would be better
- Move some parts of the service to different functions to make code cleaner and add comments
- Adding functionality which would let you have multiple offers on same product