﻿using CheckoutClientCore.Model;
using CheckoutClientCore.View;
using ShoppingService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckoutClientCore.Presenter
{
    public class ShoppingPresenter
    {
        private IShoppingView _shoppingView { get; set; }
        private IShoppingService _shoppingService { get; set; }

        public ShoppingPresenter(
            IShoppingView shoppingView,
            IShoppingService shoppingService)
        {
            _shoppingView = shoppingView;
            _shoppingService = shoppingService;

            _shoppingView.Presenter = this;

            UpdateShoppingView();
        }

        public void AddToCheckoutBasket()
        {
            if (_shoppingView.DisplayProductsIndex >= 0 && _shoppingView.DisplayProductsIndex < _shoppingView.DisplayProducts.Count)
            {
                var displayProduct = _shoppingView.DisplayProducts[_shoppingView.DisplayProductsIndex];

                var checkoutBasket = _shoppingView.Checkout;
                checkoutBasket.Add(new Product
                {
                    Sku = displayProduct.Sku,
                    Description = displayProduct.Description,
                    Price = displayProduct.Price
                });

                _shoppingView.Checkout = checkoutBasket;

                UpdateCheckoutPricing();
            }
        }

        public void RemoveFromCheckoutBasket()
        {
            if (_shoppingView.CheckoutIndex >= 0 && _shoppingView.CheckoutIndex < _shoppingView.Checkout.Count)
            {
                var checkoutBasket = _shoppingView.Checkout;
                checkoutBasket.RemoveAt(_shoppingView.CheckoutIndex);

                _shoppingView.Checkout = checkoutBasket;

                UpdateCheckoutPricing();
            }
        }

        private void UpdateShoppingView()
        {
            _shoppingView.Checkout = new List<Product>();
            var displayProducts = _shoppingService.GetDisplayProductsAsync().Result.Select(x => new DisplayProduct
            {
                Sku = x.Sku,
                Description = x.Description,
                Price = x.Price,
                SpecialOffer = x.SpecialOffer
            }).ToList();

            _shoppingView.DisplayProducts = displayProducts;
        }

        public void UpdateCheckoutPricing()
        {
            var checkoutBasket = _shoppingView.Checkout;
            List<CheckoutBalanceRequest> checkoutBalanceRequest = new List<CheckoutBalanceRequest>();

            var distincSkus = checkoutBasket.Select(x => x.Sku).Distinct();

            foreach (string sku in distincSkus)
            {
                checkoutBalanceRequest.Add(new CheckoutBalanceRequest
                {
                    Sku = sku,
                    AmountOfUnits = checkoutBasket.Count(x => x.Sku == sku)
                });
            }

            CheckoutBalanceResponce checkoutBalanceResponce = _shoppingService.GetCheckoutBalanceAsync(checkoutBalanceRequest.ToArray()).Result;

            _shoppingView.AmoundOwed = checkoutBalanceResponce.Amount;
            _shoppingView.MultiBuySavings = checkoutBalanceResponce.MultiBuySavings;
        }

        
    }
}
