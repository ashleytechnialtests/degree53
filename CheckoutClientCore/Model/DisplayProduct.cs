using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckoutClientCore.Model
{
    public class DisplayProduct
    {
        public string Sku { get; set; }
        public string Description {get; set;}
        public float Price { get; set; }
        public string SpecialOffer { get; set; }
    }
}
