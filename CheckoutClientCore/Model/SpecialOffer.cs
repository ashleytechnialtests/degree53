﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckoutClientCore.Model
{
    public class SpecialOffer
    {
        public string Sku { get; set; }
        public int Amount { get; set; }
        public float Price { get; set; }
    }
}
