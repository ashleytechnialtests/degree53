﻿using CheckoutClientCore.Model;
using CheckoutClientCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckoutClientCore.View
{
    public interface IShoppingView
    {
        List<DisplayProduct> DisplayProducts { get; set; }
        List<Product> Checkout { get; set; }

        ShoppingPresenter Presenter { set; }

        float MultiBuySavings { get; set; }
        float AmoundOwed { get; set; }

        int DisplayProductsIndex { get; set; }
        int CheckoutIndex { get; set; }
    }
}
