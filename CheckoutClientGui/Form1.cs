﻿using CheckoutClientCore.Model;
using CheckoutClientCore.Presenter;
using CheckoutClientCore.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Checkout
{
    public partial class Form1 : Form , IShoppingView
    {
        public Form1()
        {
            InitializeComponent();

            this.shoppingList.MouseDoubleClick += new MouseEventHandler(shoppingList_DoubleClick);
            this.checkout.MouseDoubleClick += new MouseEventHandler(checkout_DoubleClick);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            this.shoppingList.View = System.Windows.Forms.View.Details;
            this.shoppingList.GridLines = true;
            this.shoppingList.FullRowSelect = true;

            //Add column header
            this.shoppingList.Columns.Add("Sku");
            this.shoppingList.Columns.Add("Description");
            this.shoppingList.Columns.Add("Unit Price");
            this.shoppingList.Columns.Add("Special Offer");

            this.checkout.View = System.Windows.Forms.View.Details;
            this.checkout.GridLines = true;
            this.checkout.FullRowSelect = true;

            //Add column header
            this.checkout.Columns.Add("Sku");
            this.checkout.Columns.Add("Description");
            this.checkout.Columns.Add("Unit Price");
        }
        
        private void shoppingList_DoubleClick(object sender, EventArgs e)
        {
            if (this.shoppingList.SelectedItems[0] != null)
            {
                this.DisplayProductsIndex = this.shoppingList.SelectedItems[0].Index;
                this.Presenter.AddToCheckoutBasket();
            }
        }

        private void checkout_DoubleClick(object sender, EventArgs e)
        {
            if (this.checkout.SelectedItems[0] != null)
            {
                this.CheckoutIndex = this.checkout.SelectedItems[0].Index;
                this.Presenter.RemoveFromCheckoutBasket();
            }
        }

        public List<DisplayProduct> DisplayProducts
        {
            get
            {
                return this.shoppingList.Items.Cast<ListViewItem>()
                    .Select(item => new DisplayProduct
                    {
                        Sku = item.SubItems[0].Text,
                        Description = item.SubItems[1].Text,
                        Price = float.Parse(item.SubItems[2].Text),
                        SpecialOffer = item.SubItems[2].Text
                    }).ToList();
            }
            set
            {
                this.shoppingList.Items.Clear();

                foreach (var item in value)
                {
                    this.shoppingList.Items.Add(new ListViewItem(new string[] { item.Sku, item.Description, item.Price.ToString(), item.SpecialOffer }));
                }
            }
        }

        public List<Product> Checkout
        {
            get
            {
                return this.checkout.Items.Cast<ListViewItem>()
                    .Select(item => new Product
                    {
                        Sku = item.SubItems[0].Text,
                        Description = item.SubItems[1].Text,
                        Price = float.Parse(item.SubItems[2].Text),
                    }).ToList();
            }
            set
            {
                this.checkout.Items.Clear();

                foreach (var item in value)
                {
                    this.checkout.Items.Add(new ListViewItem(new string[] { item.Sku, item.Description, item.Price.ToString() }));
                }
            }
        }

        public ShoppingPresenter Presenter { private get; set; }

        public float MultiBuySavings
        {
            get
            {
                return float.Parse(this.savings.Text); //this shouldnt be never used
            }
            set
            {
                this.savings.Text = value.ToString();
            }
        }

        public float AmoundOwed
        {
            get
            {
                return float.Parse(this.amount.Text); //this shouldnt be never used
            }
            set
            {
                this.amount.Text = value.ToString();
            }
        }

        public int DisplayProductsIndex
        {
            get; set;
        }
        public int CheckoutIndex { get; set; }
    }
}
