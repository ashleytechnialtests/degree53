﻿using CheckoutClientCore.Presenter;
using ShoppingService;
using System;
using System.ServiceModel;
using System.Windows.Forms;

namespace Checkout
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            BasicHttpBinding myBinding = new BasicHttpBinding();

            EndpointAddress myEndpoint = new EndpointAddress("http://localhost:54782/ShoppingService.svc");

            ChannelFactory<IShoppingService> myChannelFactory = new ChannelFactory<IShoppingService>(myBinding, myEndpoint);

            IShoppingService wcfClient = myChannelFactory.CreateChannel();

            Form1 shoppingForm = new Form1();

            ShoppingPresenter shoppingPresenter = new ShoppingPresenter(shoppingForm, wcfClient);

            Application.Run(shoppingForm);
        }
    }
}
